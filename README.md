Spring Boot API Swagger Demo
=========================

Building the Application
---------------------
    $ git clone https://gitlab.com/tinhanc/api-swagger-demo.git
    $ cd api-swagger-demo
    $ mvn clean install

This demo assumes you know how to run Docker.

Building the Container
----------------------
Nothing special if you already have Docker installed:

    $ docker build -t spring-boot/api-swagger-demo .

Running the Container
---------------------
To run this container:

    $ docker run -ti spring-boot/api-swagger-demo

To view the generated Swagger UI documentation go to: [http://<host_name>:8080/swagger-ui.html](http://<host_name>:8080/swagger-ui.html)

